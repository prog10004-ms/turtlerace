"""
The Lineup module is responsible for implementing the concern dealing with racers themselves,
creating the racers and manging their attributes.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""

#access functionality from the other modules like turtles
import turtle


#declare the racing turtles variables
leo = None
mikey = None
don = None
#TODO: declare a module variable for Raphael

#define a function that will create the racing turtles
def createNinjaTurtles():
    """
    Creates and configures the racing turtles stored in module variables so that they can be
    accessed by the other modules of the application
    """
    global leo, mikey, don
    
    #create and configure Leonardo
    leo = turtle.Turtle()
    leo.shape('turtle')
    leo.color('darkgreen')
    leo.turtlesize(3,3,1)
    leo.up()

    #create and configure Michelangelo
    mikey = turtle.Turtle()
    mikey.shape('turtle')
    mikey.color('darkred')
    mikey.turtlesize(3,3,1)
    mikey.up()

    #create and configure Donatello
    don = turtle.Turtle()
    don.shape('turtle')
    don.color('blue')
    don.turtlesize(3, 3, 1)
    don.up()

    #TODO: create and configure Raphael
