"""
The Race module is responsible for implementing the concern dealing with race organization and execution
such as lining up the racers, having them race and determining the race rankings.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""

#access functionality from the other modules like turtles and random
import Lineup 
import random
import turtle

#define a function that will line-up the turtles at the starting line
def lineUpTurtles():
    """
    Positions the racing turtles at the start line ready to go. The turtles are
    200 pixels apart and they point up (North)
    """
    
    #position Leo to be the first racing turtle 200 pixels to the left of the centre
    #and points up towards the finish line
    Lineup.leo.backward(200)
    Lineup.leo.left(90)

    #position Mikey so that he points up towards the finish line
    Lineup.mikey.left(90)

    #position Don to be the third racing turtle 200 pixels to the right of the centre
    #and points up towards the finish line
    Lineup.don.forward(200)
    Lineup.don.left(90)

    #TODO: modify the lineUpTurtles function to line up Raph such that the turtles are centered 
    #on the screen and equidistant.

#define a function that will implement the race
def runRace():
    """
    Implements the race by asking the turtles to move a random distance between 0 and 100 pixels
    """
    #calculate the coordinate of the finish line
    yFinish = turtle.Screen().window_height() / 2

    #repeat turtle hops while we have not reached the finish line
    finishLineReached = False
    while not finishLineReached:
        #ask the turtles to race 
        distance = random.randint(10, 100)
        Lineup.leo.forward(distance)

        distance = random.randint(10, 100)
        Lineup.mikey.forward(distance)

        distance = random.randint(10, 100)
        Lineup.don.forward(distance)
        
        #check whether any of the turtles reached the finish line
        if Lineup.leo.ycor() >= yFinish or Lineup.mikey.ycor() >= yFinish or Lineup.don.ycor() >= yFinish:
            #race is done
            finishLineReached = True
        
        #TODO: Change this method such that turtles stop right at the finish line
        #and do not go past the edge of the window

        #TODO: Add the fourth turtle to the race

        

#define a function that will determine the winner
def determineWinner():
    """Determines the winner of the race by calculating which turtle got further"""
    #check whether leo won the race. How?
    #check leo y-coordinate is greater than mikey's and don's
    if Lineup.leo.ycor() > Lineup.mikey.ycor() and Lineup.leo.ycor() > Lineup.don.ycor():
        #leo won the race
        return Lineup.leo, "Leo"
    else: 
        #leo did not win which means it is either don or mikey
        #check whether don won the race
        if Lineup.don.ycor() > Lineup.mikey.ycor():
            #don won the race
            return Lineup.don, "Don"
        else:
            #mikey won the race
            return Lineup.mikey, "Mikey"
    #TODO: what happens if there is a tie

    #TODO: extend the algorithm for determining the winner to include a fourth turtle

#TODO: define a function that will determine rankings