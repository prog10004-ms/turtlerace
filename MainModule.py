"""
The MainModule represents the main module that must be executed first to start the Turtle Race Program
and will be responsible for the concern of interacting with the user and directing the overall sequence
of the program. 

Typically the "MainModule" is called "Program" or "Application" and represents the
application itself. For our first larger program we have called it MainModule for clarity and help us
remember that the module that is used to execute the program is automatically called __main__ by the
Python interpreter.

@Author: Prof. Magdin Stoica
@Course: Programming Priciples, PROG10004
"""

#access functionality from the other modules like turtles or the other program modules
import turtle
import Lineup
import Race

#create screen and set its colour (Main Module Concern)
raceTrack = turtle.Screen()
raceTrack.bgcolor('lightgreen')

#create the line up for the race (Lineup Module Concern)
Lineup.createNinjaTurtles()

#position the turtles at the start line using the Race module (Race Module Concern)
Race.lineUpTurtles()

#start the race (Race Module Concern)
Race.runRace()

#determine the winner of the turtle race (Race Module Concern)
winner, winnerName = Race.determineWinner()

#let the user know who won the race and  what the rankings are (Main Module Concern)
print(f'The winner of the race is {winnerName}')

#TODO: determine and print the race rankings (1st, 2nd and 3rd place)

#have the program wait for the race to complete allowing the user to watch the race (Main Module Concern)
raceTrack.exitonclick()